#include "mypainter.h"

MyPainter::MyPainter(QWidget *parent) : QMainWindow(parent)
{
	farbaPozadia = QColor(QString("white"));
	ambientneSvetlo = QColor(QString("white"));
	ui.setupUi(this);
	ui.scrollArea->setWidget(&this->paintWidget);
	ui.scrollArea->setBackgroundRole(QPalette::Dark);
	paintWidget.newImage(1500, 1500);
	ui.doubleSpinBox->hide();
	otvoritSubor("gula1.vtk");
	paintWidget.nastavPaintWidget(this, farbaPozadia);
}

void MyPainter::otvoritSubor(QString nazovSuboru)
{
	QString suborError = u8"Nepodarilo sa otvori� s�bor";

	QFile data(nazovSuboru);

	if (!data.open(QIODevice::ReadOnly)) {
		ui.statusBar->showMessage(suborError);
		return;
	}


	QTextStream text(&data);
	spracovanieSuboru(&text);
}

void MyPainter::otvoritSubor(void)
{
	QString suborError = u8"Nepodarilo sa otvori� s�bor";
	QString nazovSuboru;

	nazovSuboru = QFileDialog::getOpenFileName(this, u8"Otvori� vtk s�bor", "", u8"vtk (*.vtk)");
	
	QFile data(nazovSuboru);

	if (!data.open(QIODevice::ReadOnly)) {
		ui.statusBar->showMessage(suborError);

		return;
	}

	QTextStream text(&data);
	spracovanieSuboru(&text);
}

void MyPainter::spracovanieSuboru(QTextStream *text)
{
	/* Format suboru:
	 *
	 * 1			# vtk DataFile Version 4.1
	 * 2			vtk output
	 * 3			ASCII
	 * 4			DATASET POLYDATA
	 * 5			POINTS X float				// X po�et bodov  
	 * 6			x y z						// s�radnice bodov vo form�te [x, y, z]
	 * 5 + X		POLYGONS Y Z				// Y pocet trojuholnikov ktor� na��ta�, Z je po�et ��sel v na�om pr�pade Z = Z * 4
	 * 5 + X + 1	3 A B C						// body tvoriace trojuholn�k v porad� v akom boli na��tan�. Prv� bod m� index 0.
	 * 5 + X + Y	3 A B C						// posledn� bod
	 */

	QString riadok;
	QVector<QVector3D> body;
	QVector<QVector<int>> trojuholniky;

	text->readLine();  
	text->readLine(); 
	text->readLine(); 
	text->readLine(); 

	int pocetBodov = text->readLine().split(' ')[1].toInt();

	for (int i = 0; i < pocetBodov; i++) {
		riadok = text->readLine();
		QStringList rozdelenyRiadok = riadok.split(' ');
		QVector3D v(rozdelenyRiadok[0].toDouble(),
	    			rozdelenyRiadok[1].toDouble(),
	        		rozdelenyRiadok[2].toDouble());
		body.append(v);
	}
		
	int pocetTrojuholnikov = text->readLine().split(' ')[1].toInt();

	for (int i = 0; i < pocetTrojuholnikov; i++) {
		riadok = text->readLine();
		QStringList rozdelenyRiadok = riadok.split(' ');
		QVector<int> v;
		v.append(rozdelenyRiadok[1].toInt());
		v.append(rozdelenyRiadok[2].toInt());
		v.append(rozdelenyRiadok[3].toInt());

		trojuholniky.append(v);
	}

	e.nastavEllipsoid(body, trojuholniky, &paintWidget, paintWidget.premietanie, paintWidget.vzdialenost, ambientneSvetlo);
}

void MyPainter::skalovanie(void)
{
	int x = ui.spinBox->value();
	int y = ui.spinBox_2->value();
	int z = ui.spinBox_3->value();

	e.skalovanie(x, y, z);
}

void MyPainter::posun(void)
{
	int x = ui.spinBox->value();
	int y = ui.spinBox_2->value();
	int z = ui.spinBox_3->value();

	e.posun(x, y, z);

}

void MyPainter::zenitChange(int uhol)
{
	paintWidget.clearImage();
	e.rotaciaZenit(uhol, ui.radioButton_3->isChecked());
}

void MyPainter::azimutChange(int uhol)
{
	paintWidget.clearImage();
	e.rotaciaAzimut(uhol, ui.radioButton_3->isChecked());
}

void MyPainter::stredovePremietanie(void)
{
	paintWidget.clearImage();
	paintWidget.premietanie = 's';
	e.nastavPremietanie('s', ui.doubleSpinBox->value());
	e.zobrazEllipsoid();
	ui.doubleSpinBox->show();
}

void MyPainter::rovnobeznePremietanie(void)
{
	paintWidget.clearImage();
	paintWidget.premietanie = 'r';
	e.nastavPremietanie('r', 0.0);
	e.zobrazEllipsoid();
	ui.doubleSpinBox->hide();
}

void MyPainter::stredovaVzdialenost(double vzdialenost)
{  
	paintWidget.clearImage();
	e.nastavPremietanie('s', ui.doubleSpinBox->value() * 300);
	e.zobrazEllipsoid();
	ui.doubleSpinBox->setValue(vzdialenost);
	paintWidget.vzdialenost = vzdialenost;

}

MyPainter::~MyPainter()
{
}