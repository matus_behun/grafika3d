﻿#include "ellipsoid.h"

#define ROZSAH 360.0

ellipsoid::ellipsoid()
{
	polohaAzimut = 0;
	polohaZenit = 0;
	vykreslovanieHran = true;
	zBufferOptimalizacia = true;
	tienovanie = 's';
	QColor ambientneSvetlo(25, 25, 25);
	this->ambientneSvetlo = ambientneSvetlo;
	
	/*
	QVector3D zdroj1(-100, 0, 0);
	QColor farbazdroj1(0, 10, 0);

	QVector3D zdroj2(100, 0, 0);
	QColor farbazdroj2(0, 0, 10);
	*/

	QVector3D zdroj3(200, 200, 200);
	QColor farbazdroj3(50, 0, 0);

	
	QVector3D zdroj4(0, 500, 0);
	QColor farbazdroj4(0, 100, 0);
	

	/*
	QPair<QVector3D, QColor> p;
	p.first = zdroj1;
	p.second = farbazdroj1;

	QPair<QVector3D, QColor> q;
	q.first = zdroj2;
	q.second = farbazdroj2;
	*/

	QPair<QVector3D, QColor> r;
	r.first = zdroj3;
	r.second = farbazdroj3;

	QPair<QVector3D, QColor> s;
	s.first = zdroj4;
	s.second = farbazdroj4;

//	zdrojOsvetlenia.append(p);
//	zdrojOsvetlenia.append(q);
	zdrojOsvetlenia.append(r);
	zdrojOsvetlenia.append(s);
}

void ellipsoid::nastavEllipsoid(QVector<QVector3D> body, QVector<QVector<int>> trojuholniky, PaintWidget *p, char premietanie, double vzdialenost, QColor ambientneSvetlo)
{
		/* Inicializovanie struktur */
	paint = p;
	this->originalneBody = body;
	this->upraveneBody = body;
	this->trojuholniky = trojuholniky;
	this->premietanie = premietanie;
	this->vzdialenost = vzdialenost;

	for (int i = 0; i < trojuholniky.size(); i++) {
		for (int j = 0; j < 3; j++) {
			QSet<int> s;
			s.insert(trojuholniky[i][j]);
			s.insert(trojuholniky[i][(j + 1) % 3]);
			if (!hrany.contains(s)) {
				hrany.append(s);
			}
		}
	}

	vypocitajNormaly();
	vypocitajAmbietnuZlozku();
	vypocitajDifuznuZlozku();

	premietanie = 'r';
	zobrazEllipsoid();
}

void ellipsoid::vypocitajNormaly(void) 
{
	for(int i = 0; i < trojuholniky.size(); i++) {
		QVector3D a = upraveneBody[trojuholniky[i][0]] - upraveneBody[trojuholniky[i][1]];
		QVector3D b = upraveneBody[trojuholniky[i][0]] - upraveneBody[trojuholniky[i][2]];
		QVector3D v = -1 * QVector3D::crossProduct(a, b);
		normalyTrojuholniky.append(v);
	}

	return;
}

void ellipsoid::nastavPremietanie(char premietanie, double vzdialenost)
{
	this->premietanie = premietanie;
	this->vzdialenost = vzdialenost;
}

void ellipsoid::nastavVykreslovanieHran(bool hodnota)
{
	vykreslovanieHran = hodnota;
}

void ellipsoid::rotaciaAzimut(int novyAzimut, bool rotaciePriemetne)
{
	QVector<QVector3D> zrotovaneBody;

	double uhol = (novyAzimut / (ROZSAH));
	polohaAzimut = novyAzimut;

	for (int i = 0; i < upraveneBody.size(); i++) {	
		double y = round(double(upraveneBody[i].y() * cos(uhol)) - double(upraveneBody[i].z() * sin(uhol)));
		double z = round(double(upraveneBody[i].y() * sin(uhol)) + double(upraveneBody[i].z() * cos(uhol)));

		QVector3D v(upraveneBody[i].x(), y, z);
		zrotovaneBody.append(v);
	}

	upraveneBody = zrotovaneBody;

	if (!rotaciePriemetne) {
		vypocitajNormaly();
		vypocitajDifuznuZlozku();
	}

	zobrazEllipsoid();
}

void ellipsoid::rotaciaZenit(int novyZenit, bool rotaciaPriemetne)
{
	QVector<QVector3D> zrotovaneBody;

	double uhol = (novyZenit / (ROZSAH));
	polohaZenit = novyZenit;

	for (int i = 0; i < upraveneBody.size(); i++) {	
		double x = round(double(upraveneBody[i].x() * cos(uhol)) - double(upraveneBody[i].y() * sin(uhol)));
		double y = round(double(upraveneBody[i].x() * sin(uhol)) + double(upraveneBody[i].y() * cos(uhol)));
		QVector3D v(x, y, upraveneBody[i].z());
		zrotovaneBody.append(v);
	}

	upraveneBody = zrotovaneBody;

	if (!rotaciaPriemetne) {
		vypocitajNormaly();
		vypocitajDifuznuZlozku();
	}

	zobrazEllipsoid();
}

QVector<QPoint> ellipsoid::rovnobeznyPriemet()
{
	QVector<QPoint> premietnuteBody;

	for (int i = 0; i < upraveneBody.size(); i++) {
		int x = upraveneBody[i].x() + (paint->getWidth() / 2);
		int y = upraveneBody[i].y() + (paint->getHeight() /2);

		premietnuteBody.append(QPoint(x, y));
	}

	return premietnuteBody;
}

QVector<QPoint> ellipsoid::stredovyPriemet()
{
	QVector<QPoint> premietnuteBody;

	for (int i = 0; i < upraveneBody.size(); i++) {
		int x = (double(vzdialenost * (upraveneBody[i].x() - (paint->getHeight() / 2))) / double(vzdialenost + upraveneBody[i].z())) + paint->getHeight() / 2;
		int y = (double(vzdialenost * (upraveneBody[i].y() - (paint->getWidth() / 2))) / double(vzdialenost + upraveneBody[i].z())) + paint->getWidth() / 2;
		premietnuteBody.append(QPoint(x + paint->getWidth() / 2, y + paint->getHeight() / 2));
	}

	return premietnuteBody;
}

QVector<hrana> ellipsoid::rozdelTrojuholnik(QPair<QPoint, QVector3D> i, QPair<QPoint, QVector3D> j, QPair<QPoint, QVector3D> k)
{
	QVector<hrana> r;

		/* usporiadanie podla y */
	QPair<QPoint, QVector3D> z;
	if (i.first.y() > k.first.y()) {
		z = i;
		i = k;
		k = z;
	}

	if (i.first.y() > j.first.y()) {
		z = i;
		i = j;
		j = z;
	}

	if (j.first.y() > k.first.y()) {
		z = j;
		j = k;
		k = z;
	}

	QPoint a = i.first;
	QPoint b = j.first;
	QPoint c = k.first;
	

		/* pravouhly trojuholnik */
	if (a.y() == b.y()) {
		hrana m(i, k);
		hrana n(j, k);

		r.append(m);
		r.append(n);

		return r;
	} else if (b.y() == c.y()) {
		hrana m(k, i);
		hrana n(j, i);

		r.append(m);
		r.append(n);

		return r;
	}

	double smernica;

	if (abs(a.x() - c.x()) == 0) {
		smernica = double(a.y() - c.y() / double(DBL_MIN + 0.000001));
	} else {
		smernica = double(a.y() - c.y()) / double(a.x() - c.x());
	}
	
	double posun = a.y() - double(smernica * double(a.x()));
	int x;
	int y = b.y();
	x = round((b.y() / smernica) - (posun / smernica));

	QVector3D ikVector = k.second - i.second;
	QVector3D ikPolVector = ikVector / 2;
	QVector3D fVektor = i.second + ikPolVector;;
	QPoint fBod(x, y);

	QPair<QPoint, QVector3D> f(fBod, fVektor);

	hrana m(i, j);
	hrana n(i, f);
	hrana o(j, k);
	hrana p(f, k);

	r << m << n << o << p;

	return r;
}

void ellipsoid::scanLine(QVector<hrana> hrany, int indexTrojuholnik)
{
	QVector<int> priesecniky;

/*
	if (zBufferOptimalizacia) {
		QVector<QPair<QPoint, double>> v;
		for (int i = 0; i < hrany.size(); i++) {
			QPair<QPoint, double> p, q;
			p.first = hrany[i].dajA();
			p.second = hrany[i].dajAvektor().z();
			p.first = hrany[i].dajB();
			p.second = hrany[i].dajBvektor().z();
			v.append(p);
			v.append(q);
		}
		if (!paint->ZTest(v))
			return;
	}
*/

		
	for (int i = hrany.size() - 1; i >= 0 ; i -= 2) {
		int urovenY = hrany[i].dajDolneY();
		int hornaUrovenY = hrany[i].dajHorneY();

		QVector3D aktVectRight = hrany[i].dajAvektor();
		QVector3D aktVectLeft = hrany[i - 1].dajAvektor();
		QVector3D deltaRight = hrany[i].dajABvektor() / (hornaUrovenY - urovenY);
		QVector3D deltaLeft = hrany[i - 1].dajABvektor() / (hornaUrovenY - urovenY);

		do {
			priesecniky << hrany[i].aktualizujPriesecnik() << hrany[i - 1].aktualizujPriesecnik();
			qSort(priesecniky);

			QVector3D aktLineVect = aktVectLeft;
			QVector3D deltaLine = (aktVectRight - aktVectLeft) / abs(priesecniky[0] - priesecniky[1]);

			for (int i = priesecniky[0]; i <= priesecniky[1]; i++, aktLineVect += deltaLine) {
				if ((i >= 0 && i < paint->getWidth()) && (urovenY >= 0 && urovenY < paint->getHeight())) {
					if (aktLineVect.z() > paint->ZVzdialenost[i][urovenY]) {
						paint->ZVzdialenost[i][urovenY] = aktLineVect.z();
						if (tienovanie = 'k') {
							paint->ZPlatno[i][urovenY] = osvetli(indexTrojuholnik);
						}
						else if (tienovanie == 'g') {
							paint->ZPlatno[i][urovenY] = QColor();
						}
					}
				}
			}

			aktVectLeft -= deltaLeft;
			aktVectRight -= deltaRight;
			priesecniky.clear();
			urovenY--;
		} while (hrany[i].dajHorneY() < urovenY);
	}

}

void ellipsoid::vypocitajAmbietnuZlozku(void)
{
		/* Vypocet ambientnej zlozky */
	ambientnaZlozkaOsvetlenia = QColor((ambientnyKoeficient.red() / 255) * ambientneSvetlo.red(),
									   (ambientnyKoeficient.green() / 255) * ambientneSvetlo.green(),
							           (ambientnyKoeficient.blue() / 255) * ambientneSvetlo.blue());
}

void ellipsoid::vypocitajDifuznuZlozku(void)
{
	QColor difuznyKoeficient(100, 100, 100);

	double rKoef = double(255.0) / double(difuznyKoeficient.red());
	double gKoef = double(255.0) / double(difuznyKoeficient.green());
	double bKoef = double(255.0) / double(difuznyKoeficient.blue());

	int redDif = 0;
	int greenDif = 0;
	int blueDif = 0;

	for (int i = 0; i < trojuholniky.size(); i++) {
		QVector<QColor> trojuholnikDifuzia;
		for (int j = 0; j < zdrojOsvetlenia.size(); j++) {
			double skalarnySucin = QVector3D::dotProduct(zdrojOsvetlenia[j].first.normalized(), normalyTrojuholniky[i].normalized());
			
			if (skalarnySucin < 0) {
				skalarnySucin = 0;
			}
			
			
			redDif = zdrojOsvetlenia[j].second.red() * rKoef * skalarnySucin;
			greenDif = zdrojOsvetlenia[j].second.green() * gKoef * skalarnySucin;
			blueDif = zdrojOsvetlenia[j].second.blue() * bKoef * skalarnySucin;

			if (redDif > 255)
				redDif = 255;

			if (greenDif > 255)
				greenDif = 255;

			if (blueDif > 255)
				blueDif = 255;

			trojuholnikDifuzia.append(QColor(redDif, greenDif, blueDif));
		}
		difuznaZlozkaOsvetleniaKonst.append(trojuholnikDifuzia);
	}
}

QColor ellipsoid::osvetli(int indexTrojuholnika)
{
	int r = 0;
	int g = 0;
	int b = 0;

	r += ambientnaZlozkaOsvetlenia.red();
	g += ambientnaZlozkaOsvetlenia.green();
	b += ambientnaZlozkaOsvetlenia.blue();

	int difR = 0;
	int difG = 0;
	int difB = 0;

	for (int i = 0; i < difuznaZlozkaOsvetleniaKonst[indexTrojuholnika].size() ; i++) {
		difR += difuznaZlozkaOsvetleniaKonst[indexTrojuholnika][i].red();
		difG += difuznaZlozkaOsvetleniaKonst[indexTrojuholnika][i].green();
		difB += difuznaZlozkaOsvetleniaKonst[indexTrojuholnika][i].blue();
	}

	r += difR;
	g += difG;
	b += difB;

	if (r > 255)
		r = 255;

	if (g > 255)
		g = 255;

	if (b > 255)
		b = 255;

	return QColor(r, g, b);
}

void ellipsoid::zobrazEllipsoid()
{
	paint->resetZbuffer();
	QVector<QPoint> premietnuteBody;


	if (premietanie == 'r') {
		premietnuteBody = rovnobeznyPriemet();
	} else if(premietanie == 's') {
		premietnuteBody = stredovyPriemet();
	} else {
		return;
	}


	for (int indexTrojuholnik = 0; indexTrojuholnik < trojuholniky.size(); indexTrojuholnik++) {
		QVector<hrana> rozdelenyTrojuholnik;
		QPair<QPoint, QVector3D> a(premietnuteBody[trojuholniky[indexTrojuholnik][0]], upraveneBody[trojuholniky[indexTrojuholnik][0]]);
		QPair<QPoint, QVector3D> b(premietnuteBody[trojuholniky[indexTrojuholnik][1]], upraveneBody[trojuholniky[indexTrojuholnik][1]]);
		QPair<QPoint, QVector3D> c(premietnuteBody[trojuholniky[indexTrojuholnik][2]], upraveneBody[trojuholniky[indexTrojuholnik][2]]);


		rozdelenyTrojuholnik = rozdelTrojuholnik(a, b, c);
		scanLine(rozdelenyTrojuholnik, indexTrojuholnik);
	}

	if (vykreslovanieHran) {
		for (int i = 0; i < hrany.size(); i++) {
			QList<int> prepojenia = hrany[i].values();
			paint->Zdda(premietnuteBody.at(prepojenia.first()), premietnuteBody.at(prepojenia.last()), QColor(QString("black")));
		}
	}

	paint->vykresliZbuffer();
}

void ellipsoid::skalovanie(int x, int y, int z)
{
	for (int i = 0; i < upraveneBody.size(); i++) {
		upraveneBody[i].setX(upraveneBody[i].x() * x);
		upraveneBody[i].setY(upraveneBody[i].y() * y);
		upraveneBody[i].setZ(upraveneBody[i].z() * z);
	}

	zobrazEllipsoid();
}

void ellipsoid::posun(int x, int y, int z)
{
	for (int i = 0; i < upraveneBody.size(); i++) {
		upraveneBody[i].setX(upraveneBody[i].x() + x);
		upraveneBody[i].setY(upraveneBody[i].y() + y);
		upraveneBody[i].setZ(upraveneBody[i].z() + z);
	}

	zobrazEllipsoid();

}

ellipsoid::~ellipsoid()
{
}
