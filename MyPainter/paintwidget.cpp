#include "paintwidget.h"

#define STANDARDNA_VZDIALENOST 1.0
#define ZMENA_VZDIALENOST 0.05

PaintWidget::PaintWidget(QWidget *parent)
	: QWidget(parent)
{
	premietanie = 'r';
	vzdialenost = STANDARDNA_VZDIALENOST;
	setAttribute(Qt::WA_StaticContents);
}

bool PaintWidget::newImage(int x, int y)
{
	QImage loadedImage(x,y,QImage::Format_RGB32);
	loadedImage.fill(qRgb(255, 255, 255));
	QSize newSize = loadedImage.size();
	resizeImage(&loadedImage, newSize);
	image = loadedImage;
	this->resize(image.size());
	this->setMinimumSize(image.size());
	farbaPozadia = QColor(QString("white"));
	update();

	return true;
}

int PaintWidget::getHeight(void)
{
	return image.height();
}

int PaintWidget::getWidth(void)
{
	return image.width();
}

void PaintWidget::nastavPaintWidget(MyPainter *mp, QColor farbaPozadia)
{
	QObject::connect((QObject *) this, SIGNAL(zmenaHodnoty(double)), (QObject *) mp, SLOT(stredovaVzdialenost(double)));
	this->farbaPozadia = farbaPozadia;

}

void PaintWidget::resetZbuffer() 
{
	ZPlatno.clear();
	ZVzdialenost.clear();

	for (int i = 0; i < image.height(); i++) {
		QVector<QColor> v;
		for (int j = 0; j < image.width(); j++) {
			v.append(farbaPozadia);
		}
		ZPlatno.append(v);
	}

	for (int i = 0; i < image.height(); i++) {
		QVector<double> v;
		for (int j = 0; j < image.width(); j++) {
			v.append(-DBL_MAX);
		}
		ZVzdialenost.append(v);
	}
}

void PaintWidget::vykresliZbuffer()
{
	QPainter painter(&image);

	for (int i = 0; i < image.height(); i++) {
		for (int j = 0; j < image.width(); j++) {
			painter.setPen(QPen(ZPlatno.at(i).at(j)));
			painter.drawPoint(i, j);
		}
	}

	update();
}

void PaintWidget::Zdda(QPoint a, QPoint b, QColor c)
{
	QPoint m, n, kurzor;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	}
	else if (a.x() > b.x()) {
		m = b;
		n = a;
	}
	else {
		ZvykresliVertikalnuCiaru(a.y(), b.y(), a.x(), c);
		return;
	}

	double delta_x, delta_y, dlzka_x, dlzka_y, dkurzor_x, dkurzor_y;

	kurzor = m;
	dkurzor_x = (double)m.x();
	dkurzor_y = (double)m.y();

	dlzka_x = fabs(m.x() - n.x());
	dlzka_y = fabs(m.y() - n.y());

	if (dlzka_y <= dlzka_x) {
		delta_y = dlzka_y / dlzka_x;
		delta_x = 1;
	}
	else {
		delta_y = 1;
		delta_x = dlzka_x / dlzka_y;
	}

	if (m.y() > n.y()) {
		delta_y *= -1;
	}

	do {
		if ((kurzor.x() >= 0 && kurzor.x() < getWidth()) && (kurzor.y() >= 0 && kurzor.y() < getHeight())) {
			ZdrawPoint(kurzor.x(), kurzor.y(), 0.0, c);
		}

		dkurzor_x += delta_x;
		dkurzor_y += delta_y;
		kurzor.setX((int)round(dkurzor_x));
		kurzor.setY((int)round(dkurzor_y));
	} while (kurzor.x() != n.x() || kurzor.y() != n.y());
	if ((kurzor.x() >= 0 && kurzor.x() < 750) && (kurzor.y() >= 0 && kurzor.y() < 750)) {
		ZdrawPoint(kurzor.x(), kurzor.y(), 0.0, c);
	}
}

void PaintWidget::ZvykresliVertikalnuCiaru(int x, int y, int uroven, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(farba));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else if (x > y) {
		koniec = x;
		zaciatok = y;
	}
	else {
		return;
	}

	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(uroven, i);
	}

	update();
}

void PaintWidget::dda(QPoint a, QPoint b, QColor c)
{
	QPoint m, n, kurzor;

	if (a.x() < b.x()) {
		m = a;
		n = b;
	}
	else if (a.x() > b.x()) {
		m = b;
		n = a;
	}
	else {
		vykresliVertikalnuCiaru(a.y(), b.y(), a.x(), c);
		return;
	}

	QPainter painter(&image);
	painter.setPen(QPen(c));

	double delta_x, delta_y, dlzka_x, dlzka_y, dkurzor_x, dkurzor_y;

	kurzor = m;
	dkurzor_x = (double)m.x();
	dkurzor_y = (double)m.y();

	dlzka_x = fabs(m.x() - n.x());
	dlzka_y = fabs(m.y() - n.y());

	if (dlzka_y <= dlzka_x) {
		delta_y = dlzka_y / dlzka_x;
		delta_x = 1;
	}
	else {
		delta_y = 1;
		delta_x = dlzka_x / dlzka_y;
	}

	if (m.y() > n.y()) {
		delta_y *= -1;
	}

	do {
		painter.drawPoint(kurzor.x(), kurzor.y());
		dkurzor_x += delta_x;
		dkurzor_y += delta_y;
		kurzor.setX((int)round(dkurzor_x));
		kurzor.setY((int)round(dkurzor_y));
	} while (kurzor.x() != n.x() || kurzor.y() != n.y());
	painter.drawPoint(kurzor.x(), kurzor.y());

	update();
}

void PaintWidget::ZdrawPoint(QPoint a, double z, QColor q)
{
	if (z < ZVzdialenost[a.x()][a.y()]) {
		ZPlatno[a.x()][a.y()] = q;
	}
}

void PaintWidget::ZdrawPoint(int x, int y, double z, QColor q)
{
	if (z > ZVzdialenost[x][y]) {
		ZPlatno[x][y] = q;
		ZVzdialenost[x][y] = z;
	}
}

bool PaintWidget::ZTest(QVector<QPair<QPoint, double>> h) 
{
	for (int i = 0; i < h.size(); i++) {
		if (int(ZVzdialenost[h[i].first.x()][h[i].first.y()]) > int(h[i].second)) {
			return false;
		}
	}

	return true;
}

void PaintWidget::vykresliVertikalnuCiaru(int x, int y, int uroven, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(farba));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else if (x > y) {
		koniec = x;
		zaciatok = y;
	}
	else {
		return;
	}

	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(uroven, i);
	}

	update();
}

void PaintWidget::resizeImage(QImage *image, const QSize &newSize)
{
	if (image->size() == newSize)
		return;

	QImage newImage(newSize, QImage::Format_RGB32);
	newImage.fill(qRgb(255, 255, 255));
	QPainter painter(&newImage);
	painter.drawImage(QPoint(0, 0), *image);
	*image = newImage;
}

void PaintWidget::paintEvent(QPaintEvent *event)
{
	QPainter painter(this);
	QRect dirtyRect = event->rect();
	painter.drawImage(dirtyRect, image, dirtyRect);
}

void PaintWidget::mousePressEvent(QMouseEvent *event)
{
}

void PaintWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
}

void PaintWidget::mouseMoveEvent(QMouseEvent *event)
{
}

void PaintWidget::mouseReleaseEvent(QMouseEvent *event)
{
}

void PaintWidget::wheelEvent(QWheelEvent *event)
{
	QPoint numDegrees = event->angleDelta() / 8;

	int numChange = (numDegrees.y() / 15);

	if ((vzdialenost += numChange * ZMENA_VZDIALENOST) > 0 && premietanie == 's') {
		vzdialenost += numChange * ZMENA_VZDIALENOST;
		emit zmenaHodnoty(vzdialenost);
	}
}

void PaintWidget::clearImage()
{
	image.fill(qRgb(255, 255, 255));
	update();
}

void PaintWidget::vykresliVodorovnuCiaru(int x, int y, int uroven, QColor farba)
{
	QPainter painter(&image);
	painter.setPen(QPen(farba));

	int zaciatok;
	int koniec;

	if (x < y) {
		zaciatok = x;
		koniec = y;
	}
	else if(x > y) {
		koniec = x;
		zaciatok = y;
	} else {
		return;
	} 

	for (int i = zaciatok; i <= koniec; i++) {
		painter.drawPoint(i, uroven);
	}

	update();
}

void PaintWidget::resizeEvent(QResizeEvent *event)
{
	QWidget::resizeEvent(event);
}