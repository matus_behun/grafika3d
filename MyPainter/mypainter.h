#pragma once

#ifndef MYPAINTER_H
#define MYPAINTER_H

#include <QtWidgets/QMainWindow>
#include <QFile>
#include <qvector.h>
#include <QTextSTream>
#include <QStringList>
#include <QPair>
#include <QColor>
#include <QVector>

#include "ui_mypainter.h"
#include "paintwidget.h"
#include "ellipsoid.h"

class PaintWidget;

class MyPainter : public QMainWindow
{
	Q_OBJECT

	public:
		MyPainter(QWidget *parent = 0);
		~MyPainter();

	public slots:
		void otvoritSubor(void);
		void skalovanie(void);
		void posun(void);
		void zenitChange(int);
		void azimutChange(int);
		void stredovePremietanie(void);
		void rovnobeznePremietanie(void);
		void stredovaVzdialenost(double);

	private:
		QColor farbaPozadia;									/* inicializovat na zaciatku // doplnit do UI */
		QColor ambientneSvetlo;									/* inicializovat na zaciatku // doplnit do UI */
		QVector<QPair<QVector3D, QColor>> zdrojoveSvetla;		/* natvrdo // doplnit do UI */

		PaintWidget paintWidget;
		void otvoritSubor(QString nazovSuboru);
		void spracovanieSuboru(QTextStream *);
		Ui::MyPainterClass ui;
		ellipsoid e;
};

#endif // MYPAINTER_H