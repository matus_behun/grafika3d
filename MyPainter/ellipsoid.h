#pragma once

#include <QObject>
#include <QVector>
#include <QSet>
#include <QHash>
#include <QColor>

#include "paintwidget.h"
#include "hrana.h"

#ifndef M_PI
	#define M_PI (3.14159265358979323846)
#endif

class ellipsoid
{
	public:
		ellipsoid();
		void nastavEllipsoid(QVector<QVector3D> body, QVector<QVector<int>> hrany, PaintWidget *p, char zobrazenie, double vzdialenost, QColor ambientneSvetlo);
		void nastavPremietanie(char, double);
		void nastavVykreslovanieHran(bool);
		void rotaciaAzimut(int, bool);
		void rotaciaZenit(int, bool);
		void zobrazEllipsoid();
		void skalovanie(int, int, int);
		void posun(int, int, int);
		~ellipsoid();

	private:
		/* OSVETLENIE */
		QVector<QPair<QVector3D, QColor>> zdrojOsvetlenia;
		QColor ambientnyKoeficient;
		QColor ambientneSvetlo;

		QColor difuznyKoeficient;
		QColor zrkadlovyKoeficient;
		double odrazivost;

		QColor ambientnaZlozkaOsvetlenia;

		QVector<QVector<QColor>> difuznaZlozkaOsvetleniaKonst;
		QVector<QVector<QColor>> zrkadlovaZlozkaOsvetleniaKonst;

		QVector<QVector<QColor>> difuznaZlozkaOsvetleniaGourad;
		QVector<QVector<QColor>> zrkadlovaZlozkaOsvetleniaGourad;

		QVector<QVector3D> originalneBody;
		QVector<QVector3D> upraveneBody;
		QVector<QVector<int>> trojuholniky;
		QVector<QVector3D> normalyTrojuholniky;
		QVector<QVector3D> normalyBody;
		QVector<QSet<int>> hrany;

		PaintWidget *paint;
		int polohaAzimut;
		int polohaZenit;
		char premietanie;
		double vzdialenost;
		bool vykreslovanieHran;
		bool zBufferOptimalizacia;
		char tienovanie;


		void vypocitajNormaly(void);
		QVector<QPoint> rovnobeznyPriemet();
		QVector<QPoint> stredovyPriemet();
		QVector<hrana> rozdelTrojuholnik(QPair<QPoint, QVector3D>, QPair<QPoint, QVector3D>, QPair<QPoint, QVector3D>);
		void scanLine(QVector<hrana>, int);
		void vypocitajAmbietnuZlozku(void);
		void vypocitajDifuznuZlozku(void);
		QColor osvetli(int);

};
