/********************************************************************************
** Form generated from reading UI file 'mypainter.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MYPAINTER_H
#define UI_MYPAINTER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QScrollArea>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MyPainterClass
{
public:
    QAction *actionOpen;
    QAction *actionSave;
    QAction *actionClear;
    QAction *actionNew;
    QAction *actionNegative;
    QAction *actionSlow_Negative;
    QAction *actionMedium_Negative;
    QAction *actionSalt_Pepper;
    QAction *actionMedian_filter;
    QAction *actionRotate_left;
    QAction *actionRotate_right;
    QAction *actionOtvori;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QVBoxLayout *verticalLayout_3;
    QScrollArea *scrollArea;
    QWidget *scrollAreaWidgetContents_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_5;
    QLabel *label;
    QLabel *label_2;
    QVBoxLayout *verticalLayout_2;
    QSlider *horizontalSlider;
    QSlider *horizontalSlider_2;
    QVBoxLayout *verticalLayout_4;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QDoubleSpinBox *doubleSpinBox;
    QGridLayout *gridLayout;
    QLabel *label_4;
    QLabel *label_3;
    QSpinBox *spinBox;
    QSpinBox *spinBox_3;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QSpinBox *spinBox_2;
    QLabel *label_5;
    QRadioButton *radioButton_3;
    QRadioButton *radioButton_4;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuS_bor;

    void setupUi(QMainWindow *MyPainterClass)
    {
        if (MyPainterClass->objectName().isEmpty())
            MyPainterClass->setObjectName(QStringLiteral("MyPainterClass"));
        MyPainterClass->resize(1080, 569);
        actionOpen = new QAction(MyPainterClass);
        actionOpen->setObjectName(QStringLiteral("actionOpen"));
        actionSave = new QAction(MyPainterClass);
        actionSave->setObjectName(QStringLiteral("actionSave"));
        actionClear = new QAction(MyPainterClass);
        actionClear->setObjectName(QStringLiteral("actionClear"));
        actionNew = new QAction(MyPainterClass);
        actionNew->setObjectName(QStringLiteral("actionNew"));
        actionNegative = new QAction(MyPainterClass);
        actionNegative->setObjectName(QStringLiteral("actionNegative"));
        actionSlow_Negative = new QAction(MyPainterClass);
        actionSlow_Negative->setObjectName(QStringLiteral("actionSlow_Negative"));
        actionMedium_Negative = new QAction(MyPainterClass);
        actionMedium_Negative->setObjectName(QStringLiteral("actionMedium_Negative"));
        actionSalt_Pepper = new QAction(MyPainterClass);
        actionSalt_Pepper->setObjectName(QStringLiteral("actionSalt_Pepper"));
        actionMedian_filter = new QAction(MyPainterClass);
        actionMedian_filter->setObjectName(QStringLiteral("actionMedian_filter"));
        actionRotate_left = new QAction(MyPainterClass);
        actionRotate_left->setObjectName(QStringLiteral("actionRotate_left"));
        actionRotate_right = new QAction(MyPainterClass);
        actionRotate_right->setObjectName(QStringLiteral("actionRotate_right"));
        actionOtvori = new QAction(MyPainterClass);
        actionOtvori->setObjectName(QStringLiteral("actionOtvori"));
        centralWidget = new QWidget(MyPainterClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        scrollArea = new QScrollArea(centralWidget);
        scrollArea->setObjectName(QStringLiteral("scrollArea"));
        scrollArea->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        scrollArea->setWidgetResizable(true);
        scrollAreaWidgetContents_3 = new QWidget();
        scrollAreaWidgetContents_3->setObjectName(QStringLiteral("scrollAreaWidgetContents_3"));
        scrollAreaWidgetContents_3->setGeometry(QRect(0, 0, 1058, 278));
        scrollArea->setWidget(scrollAreaWidgetContents_3);

        verticalLayout_3->addWidget(scrollArea);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        verticalLayout_5 = new QVBoxLayout();
        verticalLayout_5->setSpacing(6);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(10, 10, 10, 10);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout_5->addWidget(label);

        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_5->addWidget(label_2);


        horizontalLayout->addLayout(verticalLayout_5);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        verticalLayout_2->setContentsMargins(10, 10, 10, 10);
        horizontalSlider = new QSlider(centralWidget);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMaximum(360);
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(horizontalSlider);

        horizontalSlider_2 = new QSlider(centralWidget);
        horizontalSlider_2->setObjectName(QStringLiteral("horizontalSlider_2"));
        horizontalSlider_2->setMaximum(360);
        horizontalSlider_2->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(horizontalSlider_2);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        verticalLayout_4->setContentsMargins(0, 0, 0, 0);
        radioButton = new QRadioButton(centralWidget);
        radioButton->setObjectName(QStringLiteral("radioButton"));
        radioButton->setChecked(true);

        verticalLayout_4->addWidget(radioButton);

        radioButton_2 = new QRadioButton(centralWidget);
        radioButton_2->setObjectName(QStringLiteral("radioButton_2"));

        verticalLayout_4->addWidget(radioButton_2);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMinimum(1);
        doubleSpinBox->setMaximum(1000);
        doubleSpinBox->setSingleStep(0.1);
        doubleSpinBox->setValue(1);

        verticalLayout_4->addWidget(doubleSpinBox);


        horizontalLayout->addLayout(verticalLayout_4);


        verticalLayout_3->addLayout(horizontalLayout);

        gridLayout = new QGridLayout();
        gridLayout->setSpacing(6);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(10, 10, 10, 10);
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));

        gridLayout->addWidget(label_4, 2, 0, 1, 1);

        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));

        gridLayout->addWidget(label_3, 1, 0, 1, 1);

        spinBox = new QSpinBox(centralWidget);
        spinBox->setObjectName(QStringLiteral("spinBox"));
        spinBox->setMinimum(-99);

        gridLayout->addWidget(spinBox, 1, 1, 1, 1);

        spinBox_3 = new QSpinBox(centralWidget);
        spinBox_3->setObjectName(QStringLiteral("spinBox_3"));
        spinBox_3->setMinimum(-99);

        gridLayout->addWidget(spinBox_3, 3, 1, 1, 1);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        gridLayout->addWidget(pushButton, 4, 1, 1, 1);

        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

        gridLayout->addWidget(pushButton_2, 4, 0, 1, 1);

        spinBox_2 = new QSpinBox(centralWidget);
        spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
        spinBox_2->setMinimum(-99);

        gridLayout->addWidget(spinBox_2, 2, 1, 1, 1);

        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));

        gridLayout->addWidget(label_5, 3, 0, 1, 1);

        radioButton_3 = new QRadioButton(centralWidget);
        radioButton_3->setObjectName(QStringLiteral("radioButton_3"));

        gridLayout->addWidget(radioButton_3, 0, 0, 1, 1);

        radioButton_4 = new QRadioButton(centralWidget);
        radioButton_4->setObjectName(QStringLiteral("radioButton_4"));
        radioButton_4->setChecked(true);

        gridLayout->addWidget(radioButton_4, 0, 1, 1, 1);


        verticalLayout_3->addLayout(gridLayout);


        verticalLayout->addLayout(verticalLayout_3);

        MyPainterClass->setCentralWidget(centralWidget);
        statusBar = new QStatusBar(MyPainterClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        MyPainterClass->setStatusBar(statusBar);
        menuBar = new QMenuBar(MyPainterClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1080, 21));
        menuS_bor = new QMenu(menuBar);
        menuS_bor->setObjectName(QStringLiteral("menuS_bor"));
        MyPainterClass->setMenuBar(menuBar);

        menuBar->addAction(menuS_bor->menuAction());
        menuS_bor->addAction(actionOtvori);

        retranslateUi(MyPainterClass);
        QObject::connect(horizontalSlider, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(zenitChange(int)));
        QObject::connect(horizontalSlider_2, SIGNAL(sliderMoved(int)), MyPainterClass, SLOT(azimutChange(int)));
        QObject::connect(radioButton, SIGNAL(clicked()), MyPainterClass, SLOT(rovnobeznePremietanie()));
        QObject::connect(radioButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(stredovePremietanie()));
        QObject::connect(doubleSpinBox, SIGNAL(valueChanged(double)), MyPainterClass, SLOT(stredovaVzdialenost(double)));
        QObject::connect(actionOtvori, SIGNAL(triggered()), MyPainterClass, SLOT(otvoritSubor()));
        QObject::connect(pushButton, SIGNAL(clicked()), MyPainterClass, SLOT(posun()));
        QObject::connect(pushButton_2, SIGNAL(clicked()), MyPainterClass, SLOT(skalovanie()));

        QMetaObject::connectSlotsByName(MyPainterClass);
    } // setupUi

    void retranslateUi(QMainWindow *MyPainterClass)
    {
        MyPainterClass->setWindowTitle(QApplication::translate("MyPainterClass", "MyPainter", Q_NULLPTR));
        actionOpen->setText(QApplication::translate("MyPainterClass", "Open", Q_NULLPTR));
        actionSave->setText(QApplication::translate("MyPainterClass", "Save", Q_NULLPTR));
        actionClear->setText(QApplication::translate("MyPainterClass", "Clear", Q_NULLPTR));
        actionNew->setText(QApplication::translate("MyPainterClass", "New", Q_NULLPTR));
        actionNegative->setText(QApplication::translate("MyPainterClass", "Negative", Q_NULLPTR));
        actionSlow_Negative->setText(QApplication::translate("MyPainterClass", "Black-White", Q_NULLPTR));
        actionMedium_Negative->setText(QApplication::translate("MyPainterClass", "Sepia", Q_NULLPTR));
        actionSalt_Pepper->setText(QApplication::translate("MyPainterClass", "Salt-Pepper", Q_NULLPTR));
        actionMedian_filter->setText(QApplication::translate("MyPainterClass", "Median filter", Q_NULLPTR));
        actionRotate_left->setText(QApplication::translate("MyPainterClass", "Rotate left", Q_NULLPTR));
        actionRotate_right->setText(QApplication::translate("MyPainterClass", "Rotate right", Q_NULLPTR));
        actionOtvori->setText(QApplication::translate("MyPainterClass", "Otvori\305\245", Q_NULLPTR));
        label->setText(QApplication::translate("MyPainterClass", "Zenit", Q_NULLPTR));
        label_2->setText(QApplication::translate("MyPainterClass", "Azimut", Q_NULLPTR));
        radioButton->setText(QApplication::translate("MyPainterClass", "Rovnobe\305\276n\303\251 prmietanie", Q_NULLPTR));
        radioButton_2->setText(QApplication::translate("MyPainterClass", "Stredov\303\251 premietanie", Q_NULLPTR));
        label_4->setText(QApplication::translate("MyPainterClass", "y", Q_NULLPTR));
        label_3->setText(QApplication::translate("MyPainterClass", "x", Q_NULLPTR));
        pushButton->setText(QApplication::translate("MyPainterClass", "Posun", Q_NULLPTR));
        pushButton_2->setText(QApplication::translate("MyPainterClass", "\305\240k\303\241lovania", Q_NULLPTR));
        label_5->setText(QApplication::translate("MyPainterClass", "z", Q_NULLPTR));
        radioButton_3->setText(QApplication::translate("MyPainterClass", "Priemet\305\210a", Q_NULLPTR));
        radioButton_4->setText(QApplication::translate("MyPainterClass", "Objekt", Q_NULLPTR));
        menuS_bor->setTitle(QApplication::translate("MyPainterClass", "S\303\272bor", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class MyPainterClass: public Ui_MyPainterClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MYPAINTER_H
