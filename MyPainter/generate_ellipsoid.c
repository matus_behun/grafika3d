#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifndef M_PI
	#define M_PI (3.14159265358979323846)
#endif

char head[] =  
"# vtk DataFile Version 4.1\n\
vtk output\n\
ASCII\n\
DATASET POLYDATA\n";

FILE *f;

void generate_ellipsoid_points(double size_x, double size_y, double size_z, int no_equators, int no_meridians) 
{	
	fprintf(f, "POINTS %d float\n", no_equators * no_meridians + 2);
		/* x-point, y-point, z-point */
	char output_format[] = "%.4lf %.4lf %.4lf\n";

	/*
	   Zvolene suradnicove osy
	
		^    z
		|y  /
		|  /
		| /
		|/-------> x
	*/

	int i, j;

	double delta_equator = (2 * size_x) / (no_equators + 1);
	double delta_meridian = (2 * M_PI) / no_meridians;


		/* Generating first point */
	fprintf(f, output_format, (double) -1 * size_x, 0.0, 0.0);
	
		/* Generate middle points */
	for(i = 0; i < no_equators; i++) {
		if(i % 2) { 
			for(j = 0; j < no_meridians; j++) {
				fprintf(f, output_format, ((double) -1 * size_x) + (i * delta_equator + delta_equator),  		
							  cos(j * delta_meridian) * size_y, 					   			
							  sin(j * delta_meridian) * size_y);
		 	}  
		} else {
			for(j = 0; j < no_meridians; j++) {
				fprintf(f, output_format, ((double) -1 * size_x) + (i * delta_equator + delta_equator),  		
							  cos(j * delta_meridian + delta_meridian / 2) * size_y, 					   			
							  sin(j * delta_meridian + delta_meridian / 2) * size_y);
			}
		} 
	}

		/* Generate end point */
	fprintf(f, output_format, (double) size_x, 0.0, 0.0);
	
}

void generate_ellipsoid_edges(int no_equators, int no_meridians) 
{
	fprintf(f, "POLYGONS %d %d\n", (2 * no_meridians + ((no_equators - 1) * no_meridians) * 2),  
				       (2 * no_meridians + ((no_equators - 1) * no_meridians) * 2) * 4); 
	char output_format[] = "%d %d %d %d\n";

	int i, j;

		/* End point edges */
	for(i = 0; i < no_meridians; i++) {
		fprintf(f, output_format, 3, 
 					  0, 
   					  (i) % no_meridians + 1, 
					  (i + 1) % no_meridians + 1
                );	
	}

		/* Iterating through "triangles are generated" */
	for(i = 0; i < no_equators - 1; i++) { 
		if(i%2) {
			for(j = 0; j < no_meridians; j++) {
				fprintf(f, output_format, 3, 
							  no_meridians * (i + 1) + ((j) % no_meridians + 1),    
							  no_meridians * i + ((j) % no_meridians + 1), 
							  no_meridians * i + ((j + 1) % no_meridians + 1)
			        );
			}
			for(j = 0; j < no_meridians; j++) {
				fprintf(f, output_format, 3, 
							  no_meridians * i + ((j + 1) % no_meridians + 1),
							  no_meridians * (i + 1) + ((j) % no_meridians + 1), 
							  no_meridians * (i + 1) + ((j + 1) % no_meridians + 1)
								 
			        );
			}
		} else {
			for(j = 0; j < no_meridians; j++) {
				fprintf(f, output_format, 3, 
							  no_meridians * (i + 1) + ((j+ 1) % no_meridians + 1),  
							  no_meridians * i + ((j) % no_meridians + 1), 
							  no_meridians * i + ((j + 1) % no_meridians + 1)
			        );
			}
			for(j = 0; j < no_meridians; j++) {
				fprintf(f, output_format, 3, 
							  no_meridians * i + ((j) % no_meridians + 1),
							  no_meridians * (i + 1) + ((j) % no_meridians + 1), 
							  no_meridians * (i + 1) + ((j + 1) % no_meridians + 1)
								 
				);
			}
		}
	}	

		/* End point edges */
	for(i = 0; i < no_meridians; i++) {
		fprintf(f, output_format, 3, 
					  no_equators * no_meridians + 1, 
					  no_meridians * (no_equators - 1) + (i) % no_meridians + 1, 
					  no_meridians * (no_equators - 1) + (i + 1) % no_meridians + 1);	
	}
}

void generate_vtk_file(double size_x, double size_y, double size_z, int no_equators, int no_meridians) 
{

	if(!(f = fopen("ellipsoid.vtk", "w"))) {
		printf("Nepodarilo sa vytvorit subor\n");

		return;
	}	

	fprintf(f, "%s", head);
	generate_ellipsoid_points(size_x, size_y, size_z, no_equators, no_meridians);
	generate_ellipsoid_edges(no_equators, no_meridians);

	close(f);
} 

int main(int argc, char *argv[])
{
	int i;
	double size_x, size_y, size_z;
	int no_equators, no_meridians;

	if(argc != 6) {
		size_x = 250.0;
		size_y = 150.0;
		size_z = 150.0;
		no_equators = 10;
		no_meridians = 10;
	} else {
		size_x = strtod(argv[1], NULL); 
		size_y = strtod(argv[2], NULL); 
		size_z = strtod(argv[3], NULL); 
		no_equators = atoi(argv[4]);
		no_meridians = atoi(argv[5]);
	}

	generate_vtk_file(size_x, size_y, size_z, no_equators, no_meridians);

	return 0;
}
