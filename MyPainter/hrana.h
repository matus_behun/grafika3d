#pragma once
#include <QObject>
#include <QPoint>
#include <math.h>
#include <QVector3D>

class hrana
{
	public:
		hrana();
		hrana(QPoint, QPoint);
		hrana(QPair<QPoint, QVector3D> x, QPair<QPoint, QVector3D> y);
		~hrana();
		int dajHorneX(void) const;
		int dajHorneY(void) const;
		int dajDolneY(void) const;
		int dajDolneX(void) const;
		void presunX(int);
		void presunY(int);
		void zmenA(QPoint);
		void zmenB(QPoint);
		void nastavHranu(QPoint, QPoint);
		QPoint dajA(void);
		QVector3D dajAvektor(void);
		QVector3D dajBvektor(void);
		QVector3D dajABvektor(void);
		QPoint dajB(void);
		/* Funkcie upravene pre potreby vyplnovacieho algoritmu */
		double dajSmernicu(void) const;
		double aktualizujPriesecnik(void);				// vracia hodnotu stareho priesecnika
		double dajPriesecnik(void);
		void vypis(void);
		static bool jeVodorovna(QPoint, QPoint);

	private:
		QPoint a;
		QPoint b;
		QVector3D aVektor;
		QVector3D bVektor;
		QVector3D abVektor;
		double smernica;
		double priesecnik;
};