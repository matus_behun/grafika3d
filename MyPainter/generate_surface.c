#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#ifndef M_PI
	#define M_PI (3.14159265358979323846)
#endif

struct bod {
	double x;
	double y;
}

char head[] =  
"# vtk DataFile Version 4.1\n\
vtk output\n\
ASCII\n\
DATASET POLYDATA\n";

FILE *f;

void generate_surface_points(double size_x, double size_y, double size_z, int no_equators, int no_meridians) 
{	
//	fprintf(f, "POINTS %d float\n", );
		/* x-point, y-point, z-point */
	char output_format[] = "%.4lf %.4lf %.4lf\n";

	/*
	   Zvolene suradnicove osy
		^    z
		|y  /
		|  /
		| /
		|/-------> x
	*/

	int i, j;

//	double delta_equator = ;
//	double delta_meridian = ;


		/* Generate middle points */
	for(i = 0; i < no_equators; i++) {
	}
}

void generate_surface_edges(int no_equators, int no_meridians) 
{
//	fprintf(f, "POLYGONS %d %d\n", );
	char output_format[] = "%d %d %d %d\n";

	int i, j;

	for(i = 0; i < no_equators - 1; i++) { 
	}	
}

void generate_vtk_file(double size_x, double size_y, double size_z, int no_equators, int no_meridians) 
{

	if(!(f = fopen("surface.vtk", "w"))) {
		printf("Nepodarilo sa vytvorit subor\n");

		return;
	}	

	fprintf(f, "%s", head);
	generate_surface_points(size_x, size_y, size_z, no_equators, no_meridians);
	generate_surface_edges(no_equators, no_meridians);

	close(f);
} 

int main(int argc, char *argv[])
{
	int i;

	no_equators = atoi(argv[0]);
	no_meridians = atoi(argv[1]);
	for(i = 3; i < argc; i++) {
		printf("%s\n", argv[i]);
	}

	return 0;
}