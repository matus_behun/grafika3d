#pragma once

#ifndef PAINTWIDGET_H
#define PAINTWIDGET_H

#include <QColor>
#include <QImage>
#include <QPoint>
#include <QWidget>
#include <QtWidgets>
#include <QMessageBox>
#include <random>
#include <algorithm>

class MyPainter;

class PaintWidget : public QWidget
{
	Q_OBJECT

	public:
		PaintWidget(QWidget *parent = 0);
		void nastavPaintWidget(MyPainter * mp, QColor);
		void dda(QPoint a, QPoint b, QColor c);

			/* Zbuffer */
		void resetZbuffer();
		void vykresliZbuffer();
		void Zdda(QPoint a, QPoint b, QColor c);
		void ZvykresliVertikalnuCiaru(int x, int y, int uroven, QColor farba);
		void ZdrawPoint(QPoint a, double z, QColor q);
		void ZdrawPoint(int x, int y, double z, QColor q);
		bool ZTest(QVector<QPair<QPoint, double>> h);

		QVector<QVector<QColor>> ZPlatno;
		QVector<QVector<double>> ZVzdialenost;

		int getHeight(void);
		int getWidth(void);
		char premietanie;
		double vzdialenost;
		bool newImage(int x, int y);
		void clearImage();
		void vykresliVodorovnuCiaru(int x, int y, int uroven, QColor farba);
	signals:
		void zmenaHodnoty(double);
	protected:
		void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
		void resizeEvent(QResizeEvent *event) Q_DECL_OVERRIDE;
		void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
		void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE; 
		void wheelEvent(QWheelEvent *event) Q_DECL_OVERRIDE;
	private:
		QColor farbaPozadia;
		QImage image;
		void vykresliVertikalnuCiaru(int x, int y, int uroven, QColor farba);
		void resizeImage(QImage *image, const QSize &newSize);
};

#endif // PAINTWIDGET_H